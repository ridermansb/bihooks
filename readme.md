*Compile* [![Build Status](http://ci.bindsolution.com/buildStatus/icon?job=bihooks)](http://ci.bindsolution.com/job/bihooks/) *Publish* [![Build Status](http://ci.bindsolution.com/buildStatus/icon?job=bihooks.publish)](http://ci.bindsolution.com/job/bihooks.publish/)

# Humans

Humans.txt é um arquivo texto responsável por descrever todos aqueles envolvidos no projeto.  
Segue um padrão bastante utilizado descrito em [humanstxt.org](http://humanstxt.org/Standard.html).

# EditorConfig

[EditorConfig](http://editorconfig.org/) ajuda os desenvolvedores a definir e manter estilos de codificação consistentes entre os diferentes editores e IDEs . O projeto consiste em um formato de arquivo para a definição de estilos de codificação para o editor de texto que permitem aos editores para ler o formato de arquivo e aderir a estilos definidos como espaçamentos. O Padrão escolhido para o projeto é de estilo de identação em tabs com 4 espaços.

# Gerenciamento de Pacotes

O Projeto utiliza o [Bower](http://bower.io/) para gerenciar pacotes javascripts e [Nuget](http://www.nuget.org/) para pacotes .net.  

Os pacotes .net e .js (originados do Bower) **não** devem ser comitados (*enviados para o repositório*), utilize o [Package Restore](http://docs.nuget.org/docs/workflows/using-nuget-without-committing-packages) disponível a partir do nuget 2.7 e `bower install` para restaura-los.

## Bower

Bower é um gerenciado de pacotes javascrit, com ele podemos gerenciar pacotes e suas dependências.  
A instação é bem simples (`npm install -g bower`), tendo como pré-requisito o [node.js](http://nodejs.org/) instalado na máquina.

Test #17

