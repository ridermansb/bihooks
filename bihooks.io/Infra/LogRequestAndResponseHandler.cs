﻿using log4net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace bihooks.io.Infra
{
	public class LogRequestAndResponseHandler : DelegatingHandler
	{
		private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		protected override async Task<HttpResponseMessage> SendAsync(
			HttpRequestMessage request, CancellationToken cancellationToken)
		{
			//logging request body
			string requestBody = await request.Content.ReadAsStringAsync();
			log.Info(requestBody);

			//let other handlers process the request
			return await base.SendAsync(request, cancellationToken)
				.ContinueWith(task =>
				{
					//once response is ready, log it
					var responseBody = task.Result.Content;
					log.Info(responseBody);

					return task.Result;
				});
		}
	}
}