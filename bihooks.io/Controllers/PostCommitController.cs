﻿using log4net;
using System.Collections.Generic;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web.Http;
using TrelloNet;

namespace bihooks.io.Controllers
{
    public class PostCommitController : ApiController
    {
		private const string REGEX_COMMIT = @"tc\((?<idtc>[a-zA-Z0-9]{8})\)|#(?<idhash>[a-zA-Z0-9]{8})$|^(https?:\/\/trello\.com\/c\/)(?<idurl>[\/\w \.-]*)*\/?$";
		private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		[HttpPost]
		public void TrelloUpdate(PostCommitGitData data)	
		{
			if (data == null) log.Warn("data is null!"); 

			var urlMarkdownFormater = "[{0}](" + data.repository.website + "/commits/{0})";
			ITrello trello = new Trello(ConfigurationManager.AppSettings["TrelloKey"].ToString());
			trello.Authorize(ConfigurationManager.AppSettings["TrelloToken"].ToString());

			foreach (var commit in data.commits)
			{
				
				log.Info("Processing commit #" + commit.node);

				var maches = Regex.Matches(commit.message, REGEX_COMMIT);
				var msg = "from commit: " + string.Format(urlMarkdownFormater, commit.raw_node) + "   " + commit.message;

				foreach (var cardsId in maches)
				{
					var mach = (Match)cardsId;
					var group = (Group)(mach.Groups["idtc"] ?? mach.Groups["idhash"] ?? mach.Groups["idurl"]);
					var card = trello.Cards.WithId(group.Value);

					log.Info("Add comment for " + card.Name + " #" + group.Value);
					trello.Cards.AddComment(card, msg);
				}
			}

			trello.Deauthorize();
		}

    }



	public class File
	{
		public string file { get; set; }
		public string type { get; set; }
	}

	public class Commit
	{
		public string author { get; set; }
		public string branch { get; set; }
		public List<File> files { get; set; }
		public string message { get; set; }
		public string node { get; set; }
		public List<string> parents { get; set; }
		public string raw_author { get; set; }
		public string raw_node { get; set; }
		public object revision { get; set; }
		public int size { get; set; }
		public string timestamp { get; set; }
		public string utctimestamp { get; set; }
	}

	public class Repository
	{
		public string absolute_url { get; set; }
		public bool fork { get; set; }
		public bool is_private { get; set; }
		public string name { get; set; }
		public string owner { get; set; }
		public string scm { get; set; }
		public string slug { get; set; }
		public string website { get; set; }
	}

	public class PostCommitGitData
	{
		public string canon_url { get; set; }
		public List<Commit> commits { get; set; }
		public Repository repository { get; set; }
		public string user { get; set; }
	}


	public class Payload
	{
		public string canon_url { get; set; }
		public List<Commit> commits { get; set; }
		public Repository repository { get; set; }
		public string user { get; set; }
	}
}
