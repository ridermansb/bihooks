﻿using Jil;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace bihooks.io
{
	/// <seealso cref="http://blog.developers.ba/replace-json-net-jil-json-serializer-asp-net-web-api/"/>
	public class JilFormatter : MediaTypeFormatter
	{
		private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		private readonly Options _jilOptions;

		public JilFormatter()
		{
			_jilOptions = new Options(dateFormat: DateTimeFormat.ISO8601);
			SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/json"));

			SupportedEncodings.Add(new UTF8Encoding(encoderShouldEmitUTF8Identifier: false, throwOnInvalidBytes: true));
			SupportedEncodings.Add(new UnicodeEncoding(bigEndian: false, byteOrderMark: true, throwOnInvalidBytes: true));
		}
		public override bool CanReadType(Type type)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			return true;
		}

		public override bool CanWriteType(Type type)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			return true;
		}

		public override Task<object> ReadFromStreamAsync(Type type, Stream readStream, System.Net.Http.HttpContent content, IFormatterLogger formatterLogger)
		{
			return Task.FromResult(this.DeserializeFromStream(type, readStream));
		}


		private object DeserializeFromStream(Type type, Stream readStream)
		{
			using (var reader = new StreamReader(readStream))
			{
				try
				{

					MethodInfo method = typeof(JSON).GetMethod("Deserialize", new Type[] { typeof(TextReader), typeof(Options) });
					MethodInfo generic = method.MakeGenericMethod(type);
					return generic.Invoke(this, new object[] { reader, _jilOptions });
				}
				catch
				{
					var txt = reader.ReadToEnd();
					log.Warn("Try second time deserialize json: " + txt);	
					txt = "{" + txt + "}";
					using (var readerAlt = new StringWriter(new StringBuilder(txt)))
					{
						MethodInfo method = typeof(JSON).GetMethod("Deserialize", new Type[] { typeof(TextReader), typeof(Options) });
						MethodInfo generic = method.MakeGenericMethod(type);
						return generic.Invoke(this, new object[] { readerAlt, _jilOptions });
					}

				}
			}


		}


		public override Task WriteToStreamAsync(Type type, object value, Stream writeStream, System.Net.Http.HttpContent content, TransportContext transportContext)
		{
			using (TextWriter streamWriter = new StreamWriter(writeStream))
			{
				JSON.Serialize(value, streamWriter, _jilOptions);
				return Task.FromResult(writeStream);
			}
		}
	}
}