﻿using bihooks.io.Infra;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;

namespace bihooks.io
{
	internal class WebApiConfig
	{
		public static void Register(HttpConfiguration config)
		{
			config.Services.Add(typeof(IExceptionLogger), new TraceSourceExceptionLogger());
			config.Routes.MapHttpRoute(
					name: "Default",
					routeTemplate: "{controller}/{action}"
				);

			config.Formatters.RemoveAt(0);
			config.Formatters.Insert(0, new JilFormatter());

			config.MessageHandlers.Add(new LogRequestAndResponseHandler());

		}
	}

	public class TraceSourceExceptionLogger : ExceptionLogger
	{
		public override void Log(ExceptionLoggerContext context)
		{
			Elmah.ErrorSignal.FromCurrentContext().Raise(context.Exception);
		}
	}
}